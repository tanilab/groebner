(in-package :cl)
(defpackage :earlgray.operator
  (:use :cl)
  (:export add +.
	   substruct -.
	   multiply *.
	   divide /.
	   power ^.))
(in-package :earlgray.operator)

(defgeneric add (a b))
(defgeneric substruct (a b))
(defgeneric multiply (a b))
(defgeneric divide (a b))
(defgeneric power (a b))

(setf (fdefinition '+.) (lambda (&rest args) (reduce #'add args)))
(setf (fdefinition '-.) (lambda (&rest args) (reduce #'substruct args)))
(setf (fdefinition '*.) (lambda (&rest args) (reduce #'multiply args)))
(setf (fdefinition '/.) (lambda (&rest args) (reduce #'divide args)))
(setf (fdefinition '^.) #'power)
